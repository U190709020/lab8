package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle {
    int h;

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    @Override
    public String toString() {
        return "Cylinder{" + "h=" + h + "r="+ getR() + '}';
    }

    public Cylinder(int Radius, int h) {
        super(Radius);
        this.h = h;
    }

    public double Volume(){
        return   Math.PI * Math.pow(getR(),2) * h;
    }
    public  double Area(){
        return 2 * Math.PI * getR() * (getR() + h);
    }
}
