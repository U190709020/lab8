package shapes3d;

import shapes2d.Square;

public class Cube extends Square {

    public Cube(int side) {
        super(side);
    }

    @Override
    public String toString() {
        return "Cube{" + "side=" + getSide() + "}";
    }

    public double Volume(){
        return Math.pow(getSide(),3);
    }

    public double Area(){
        return 6 * Math.pow(getSide(),2);
    }
}
