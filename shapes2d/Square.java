package shapes2d;

public class Square {
    private int side;

    public int getSide() {
        return side;
    }

    public void setSide(int side) {
        this.side = side;
    }

    public Square(int side){
        this.side = side;
    }

    public double Area(){
        return Math.pow(side,2);
    }

    @Override
    public String toString() {
        return "Square{" + "side=" + side + '}';


    }
}
