package shapes2d;

public class Circle {
    private int R;

    public int getR() {
        return R;
    }

    public void setR(int r) {
        R = r;
    }

    public Circle(int Radius){
        this.R = Radius;
    }

    @Override
    public String toString() {
        return "Circle{" + "R=" + R + '}';


    }

    public double Area(){
        return Math.PI * Math.pow(R,2);
    }

}
